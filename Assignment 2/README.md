# Game Studio 2
Rainbow Unicorns Assignment 2

Team members:
Kristine Wong Yeh Kay s3587133
Rebecca DiLella s3663787

=====================================================================================================================================================================================================
Contributions:
	Assignment 2:
		Kristine:
			Extended the text reaction and text reaction editor to include an array of text to allow characters to react with random lines
			Added some extra reactions/conditions for the bear and bunny			

		Rebecca:
			Added some extra reactions/conditions for the Cat and the display in the new room
			Made the new room by importing the security room into Maya and rearranging a bunch of assets
			Added the 'CombineInInventory' class to allow players to combine items in their inventory to solve inventory puzzles	
					
====================================================================================================================================================================================================			
Downloads:
	Rabbit Model: Hanny&Nanny by Mellowmesher <https://www.turbosquid.com/FullPreview/Index.cfm/ID/1028231>
	Bear Model: Knut Free 3D Model by ck1 <https://www.cgtrader.com/free-3d-models/sports/toy/knut>
	Cat Model & Animations: Cartoon Cat by Zealous Interactive <https://assetstore.unity.com/packages/3d/characters/animals/cartoon-cat-70180> 
	Cat Sound Effect: Cat, Screaming, A.wav by InspectorJ <https://freesound.org/people/InspectorJ/sounds/415209/>
	Book: Druid Tome, by Inner Drive Studios <https://assetstore.unity.com/packages/3d/props/druid-tome-107953>