﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 *
 *  Author: Rebecca Dilella s3663787 
 *
 */

/////////////////////////////////////////////////////////////////////////////////////
/// Class for handling the combining of items in the inventory.                   ///
/// Once an item is picked a small sprite of it is displayed next to the pointer  ///
/// to show that it has been picked, clicking elsewhere in the screen or on an    ///
/// empty item slot, clears the item and disables the sprite, clicking on         ///
/// another item, causes the script to compare the items to see if they are       ///
/// combinable, if they are, the items are combined, if they're not, the items    ///
/// are cleared                                                                   ///
//////////////////////////////////////////////////////////////////////////////////////

public class CombineItemsInInventory : MonoBehaviour {

    /////////// references to the object that will display a sprite next to the mouse///////
    public GameObject MouseIconObj;
    private Image mouseIconImage;
    private readonly Vector3 mouseImageOffset = new Vector3(-8, -16, 0);

    private Inventory inventory; 

    private GameObject pickedItemObject;
    private Item pickedItem;
    private int indexOfItem;
    
    private Item targetItem;
    private Item combinedItem;
    
    private void Start() {
        mouseIconImage = MouseIconObj.GetComponent<Image>();
        inventory = FindObjectOfType<Inventory>();
    }

    private void Update() {
        MouseIconObj.transform.position = Input.mousePosition + mouseImageOffset;
    }
    

    //////////////////////////////////////////////////////////////////////////////////
    /// called by an event trigger on the item image in an inventory slot          ///
    /// first checks if the user has already clcked on an inventory item, if not,  ///
    /// it assigns the item as the picked item, if there's already a picked item,  ///
    /// it assigns a target item andchecks if the picked and target items are      ///
    /// compatable , if they are it combines them in the inventory, if not,        ///
    /// it clears all assigned variables                                           ///
    //////////////////////////////////////////////////////////////////////////////////

    public void OnPointerClick(BaseEventData data) {
        /////finds whitch item the user clicked on/////
        PointerEventData pData = (PointerEventData) data;
        GameObject currentTarget = pData.pointerCurrentRaycast.gameObject;

        if (pickedItemObject == null) {
            PickUpInventoryItem(currentTarget);
        }
        else if(currentTarget != pickedItemObject){
            AssignTargetItem(currentTarget);
            if (CheckCompatableItems()) {
                CombineCompatableItems();
            }
            else {
                ClearItems();
            }
        }
        else {
            ClearItems();
        }
    }


    //////////////////////////////////////////////////////////////////////////////////
    /// this method takes in a game object and assigns it as the 'picked up' item  ///
    /// it then assigns all the relevant components of the object for later use    ///
    //////////////////////////////////////////////////////////////////////////////////
    
    private void PickUpInventoryItem(GameObject target) {
        pickedItemObject = target;
        
        ///////for displaying the icon next to the mouse///////////
        mouseIconImage.sprite = target.GetComponent<Image>().sprite;
        MouseIconObj.SetActive(true);

        /////////for finding the index of the item using its sprite/////////
        foreach (Image image in inventory.itemImages) {
            if (image.sprite == mouseIconImage.sprite) {
                indexOfItem = Array.IndexOf(inventory.itemImages, image);
            }            
        }

        /////////retrieviing the actaul item////////////////
        pickedItem = inventory.items[indexOfItem];
       
    }


    //////////////////////////////////////////////////////////////////////////////////
    /// this method takes in a game object and assigns it as the 'target' item     ///
    //////////////////////////////////////////////////////////////////////////////////

    private void AssignTargetItem(GameObject target) {

        ///////finds the index of the item using its sprite////////
        Image targetImage = target.GetComponent<Image>();

        foreach (Image image in inventory.itemImages) {
            if (image.sprite == targetImage.sprite) {
                indexOfItem = Array.IndexOf(inventory.itemImages, image);
            }            
        }
        ////////assigns the actual item using the index//////////
        targetItem = inventory.items[indexOfItem];
    }


    //////////////////////////////////////////////////////////////////////////////////////
    /// this method checks compatability by comparing the array of items on each item  ///
    /// if it finds a matching one, it returns true, and assigns the matched item to   ///
    /// be the combuned item; otherwise it returns false                               ///
    //////////////////////////////////////////////////////////////////////////////////////

    private bool CheckCompatableItems() {
        /////if either item doesn't have any items it can combine into, retrn false///////
        if (pickedItem.possibleItemCombinations == null || targetItem.possibleItemCombinations == null) {
            return false;
        }

        /////checks each item in the first array against every item in the second//////////
        foreach (Item pickedComboItem in pickedItem.possibleItemCombinations) {
            foreach (Item targetComboItem in targetItem.possibleItemCombinations) {
                ////if a match is found, assign the matching item as 'combined item' and return true////
                if (pickedComboItem == targetComboItem) {
                    combinedItem = pickedComboItem;
                    return true;
                }
            }
        }
        /////if no match was found, return false//////
        return false;
    }


    ///////////////////////////////////////////////////////////////////////////////////
    /// this method actually 'combines' the items; It removes the picked and target ///
    /// items from the inventory, and adds the combined item. if the combined item  ///
    /// has an associated condition, the condition is changed                       ///
    ///////////////////////////////////////////////////////////////////////////////////
    
    private void CombineCompatableItems() {
        
        inventory.RemoveItem(pickedItem);
        inventory.RemoveItem(targetItem);
        inventory.AddItem(combinedItem);

        /////if there is a condition to change, change it from what it currenty is///////
        if(combinedItem.associatedCondition != null)
            combinedItem.associatedCondition.satisfied = !combinedItem.associatedCondition.satisfied;

        ClearItems();
    }

    //////////////////////////////////////////////////////////////////////////////////
    /// this method clears all the assigned variables                              ///
    ///this method is public because it is called by event triggers on empty       ///
    /// inventory slots, and within the player movement script, so that the items  ///
    /// and sprites are cleared while the player is interacting with other parts   ///
    /// of the game                                                                ///
    //////////////////////////////////////////////////////////////////////////////////
   
    public void ClearItems() {
        MouseIconObj.SetActive(false);
        pickedItemObject = null;
        pickedItem = null;
        mouseIconImage.sprite = null;
        targetItem = null;
        combinedItem = null;

    }

}
