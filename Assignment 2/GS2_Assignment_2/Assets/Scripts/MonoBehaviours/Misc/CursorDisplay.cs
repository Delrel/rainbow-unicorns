﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * Author: Rebecca Dilella s3663787
 *
 */

public class CursorDisplay : MonoBehaviour {
    //////////////////////////////////////////////////////////////////////////////////////////////////
    ///// Class for displaying the cursor, using the built in event triggers, a default custom    ////
    ///// cursor displays by default, when the pointer hovers over an interactable, a different   ////
    ///// texture replaces it, until the pointer moves away                                       ////
    //////////////////////////////////////////////////////////////////////////////////////////////////

    public Texture2D cursorDefault; 
    public Texture2D cursorHover;

    //set the cursor to the default texture, by default
	void Start () {
	    Cursor.SetCursor(cursorDefault, Vector2.zero, CursorMode.Auto);
	}

    //called by an event trigger on the interactable objects to set the cursor to the "hover" texture
    public void OnPointerEnter() {
        Cursor.SetCursor(cursorHover, Vector2.zero, CursorMode.Auto);
    }

    //called by an event trigger on the interactable objects to reset the cursor to default
    public void OnPointerExit() {
        Cursor.SetCursor(cursorDefault, Vector2.zero, CursorMode.Auto);
    }
    

}
