﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// This simple script represents Items that can be picked
// up in the game.  The inventory system is done using
// this script instead of just sprites to ensure that items
// are extensible.
[CreateAssetMenu]
public class Item : ScriptableObject
{
    public Sprite sprite;

  /*
   *
   * extended by Kristine Wong s3587133 and Rebecca Dilella s3663787
   *
   */
   
	public string itemName;
	public string itemDesc;

    //////////////////////////////////////////////////////////////////////////////////
    /// an array of items that can be checked against another item's array         ///
    /// if they both contain the same item they can be combined, I made            ///
    /// it an array because I might want to add items that can combine             ///  
    /// with multiple other items in the future                                    ///
    ///                             -Bec-                                          ///
    //////////////////////////////////////////////////////////////////////////////////
    public Item[] possibleItemCombinations;

    public Condition associatedCondition;
}

