﻿using UnityEngine;

// This Reaction has a delay but is not a DelayedReaction.
// This is because the TextManager component handles the
// delay instead of the Reaction.


/*
 *
 * extended by Kristine Wong s3587133
 *
 */

public class TextReaction : Reaction
{
    public string[] messages = new string[1];   // The text to be displayed to the screen. Default the array to 1, because if you want to use a text reaction, you want at least one line
    public Color textColor = Color.white;       // The color of the text when it's displayed (different colors for different characters talking).
    public float delay;                         // How long after the React function is called before the text is displayed.


    private TextManager textManager;            // Reference to the component to display the text.


    protected override void SpecificInit()
    {
        textManager = FindObjectOfType<TextManager> ();
    }

    /// <summary>
    /// Replaced the single string in the reaction with an array of strings
    /// If there is only one string in the array that one will always be displayed in the reaction
    /// otherwise a random string form the array will be selected to be displayed
    /// </summary>


    protected override void ImmediateReaction()
    {
        //if there are no messages to display, return
        if (messages.Length == 0)
            return;

        var randomMessage = messages[Random.Range(0, messages.Length)];
        textManager.DisplayMessage(randomMessage, textColor, delay);

    }
}