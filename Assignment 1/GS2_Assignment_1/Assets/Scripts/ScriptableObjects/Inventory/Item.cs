﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// This simple script represents Items that can be picked
// up in the game.  The inventory system is done using
// this script instead of just sprites to ensure that items
// are extensible.
[CreateAssetMenu]
public class Item : ScriptableObject
{
    public Sprite sprite;

  /*
   *
   * extended by Kristine Wong s3587133 
   *
   */
   
	public string itemName;
	public string itemDesc;
}

