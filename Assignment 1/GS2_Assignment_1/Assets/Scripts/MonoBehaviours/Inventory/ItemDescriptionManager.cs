﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 *
 *  Author: Rebecca Dilella s3663787 
 *
 */

public class ItemDescriptionManager : MonoBehaviour {

    //////////////////////////////////////////////////////////////////////////////////////////////////
    ///// Class for handling the UI Display of the descriptions and names of the inventory items  ////
    ///// it uses the built in event triggers to display the information when the pointer hovers  ////
    ///// over the inventory slot, and hide it when the pointer leaves                            ////
    //////////////////////////////////////////////////////////////////////////////////////////////////

    public AnimationCurve OpenCurve;
    public AnimationCurve CloseCurve;

    private RectTransform currentRect;
    private float dynamicWidth;
    private Vector3 dynamicSize;
    
    private Text[] texts; //collection for text components to show/hide
    

    ///////////////////////////////////////////////////////////////////////////////
    /// called by an event trigger on the item image in an inventory slot       ///
    /// retrieves the text associated with the scriptable object that was set   ///
    /// by the Inventory script, and the image used as background for the text  ///
    /// and makes them visible                                                  ///
    ///////////////////////////////////////////////////////////////////////////////

    public void OnPointerEnter(BaseEventData data) {

        //retrieve which inventory slot the pointer is currently pointing at 
        PointerEventData pData = (PointerEventData) data;
        GameObject currentTarget = pData.pointerCurrentRaycast.gameObject;

        //Text[] currentTexts 
        texts = currentTarget.GetComponentsInChildren<Text>(); 
        //texts = currentTexts; //the two text components associated with the slot - stored in texts to use later in OnPointerExit

        currentRect = currentTarget.transform.GetChild(0).GetComponent<RectTransform>(); //the RectTransform of the background image for the text
        
        StartCoroutine(FoldOut(0.5f, true, currentRect, texts)); //display the text background

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// called by an event trigger on the item image hides the background and text when the pointer leaves ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    public void OnPointerExit() {

        StartCoroutine(FoldOut(0.5f, false, currentRect, texts));//hide the background

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// coroutine used  to animate the UI popup of the name/description of the inventory item, takes a float to set ///
    /// the amount of time it takes to complete, the RectTransform it needs to animate, and a bool dictating if the ///
    /// animation should be showing or hiding the information. Uses one of two animation curves to set the scale of ///
    /// the rect transform and the alpha of the texts                                                               ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    IEnumerator FoldOut(float stopTime, bool opening, RectTransform rect, Text[] coTexts) {

        float timer = 0;

        AnimationCurve curve = opening ? OpenCurve : CloseCurve;

        while (timer <= stopTime) {

            timer += Time.deltaTime;

            dynamicWidth = curve.Evaluate(timer / stopTime);
            dynamicSize = new Vector3(dynamicWidth, 1, 1);
            rect.localScale = dynamicSize;

            foreach (Text text in coTexts) {
                text.color = Color.white * curve.Evaluate(timer / stopTime);
            }

            yield return null;
        }
    }
}
