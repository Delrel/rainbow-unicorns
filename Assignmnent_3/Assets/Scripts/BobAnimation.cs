﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

///////////////////////////////////////////////////////////////////
/// Super quick animation for the title sprite using a sin wave ///
///////////////////////////////////////////////////////////////////

public class BobAnimation : MonoBehaviour {


	void Update () {
	    transform.position = new Vector3(transform.position.x, transform.position.y + (0.1f * Mathf.Sin(2*Time.time)), transform.position.z);
	}
}
