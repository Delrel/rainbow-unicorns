﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour: Kristine Wong Yeh Kay s3587133
 *
 */

///////////////////////////////////////////////////////
/// This class displays a blocks hp over the block ///
//////////////////////////////////////////////////////

public class HPdisp : MonoBehaviour {

    
    private int HPleft;
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

        Block block = gameObject.GetComponentInParent<Block>();

        HPleft = block.hitsRemaining;

        transform.GetComponent<TextMesh>().text = HPleft.ToString();


	}
}
