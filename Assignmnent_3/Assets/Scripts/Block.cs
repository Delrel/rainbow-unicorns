﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

////////////////////////////////////////////////////////////////////////////////////
/// This class handles the behaviour of the blocks in the game , it registers    ///
/// collisions for the blocks and keeps track of the blocks health. It triggers  ///
/// events when a bliock is hit, and when a block is destroyed. It is a child of ///
/// the class Spawnable, which includes a method that moves the blocks down at   ///
/// the end of a round                                                           ///
////////////////////////////////////////////////////////////////////////////////////

public class Block : Spawnable {
    //The delegate and event that fires when a block is hit 
    public delegate void BlockHit();
    public static  event BlockHit OnBlockkHit;

    //The delegate and event that fires when a block is hit
    public delegate void BlockDestroyed();
    public static  event BlockDestroyed OnBlockDestroyed;

    //animation curves for animating the blocks
    public AnimationCurve HitAnimationCurve;
    public AnimationCurve DestroyAnimationCurve;

    public int hitsRemaining; //the int used to keep track of the blocks hit points
    private Material blockMaterial; //to store the instance of the blocks material, used to update its colour
    private bool destroying; //bool returns true when the block is being destroyed

    private void Awake () {
	    blockMaterial = GetComponent<MeshRenderer>().material; //retieving the instance of the material on the block
        particleEffects = FindObjectOfType<ParticleEffects>(); //finding the object used to fire particle effects (the variable 'particleEffects' is inherited from spawnable)
    }
    

    //////////////////////////////////////////////////////////////////////////////////
    /// this method changes the colour of the blocks material based on its hit     ///
    /// points, it does this by lerping between the colours white and grey, the    ///
    /// float is determined using the hitsRemaining                                ///
    //////////////////////////////////////////////////////////////////////////////////
	private void UpdateBlockColour () {
	    blockMaterial.color = Color.Lerp(Color.white, Color.grey, hitsRemaining / 10f);
	}

    //////////////////////////////////////////////////////////////////////////////////
    /// this is the collision event that is called when the block registers a      ///
    /// colision it decrements the hitsRemaining on the block, and depending on the///
    /// hits it has left after that, either updates its colour, or destroys        /// 
    /// the block, it also calls the coroutines that animate the block             ///
    //////////////////////////////////////////////////////////////////////////////////

    private void OnCollisionEnter (Collision other) {
        hitsRemaining--;

        if (hitsRemaining <= 0) {//if the block needs to be destroyed
            //fire the OnBlockDetroyed event 
            if (OnBlockDestroyed != null)
                OnBlockDestroyed();
            //start the destroy block animation
            StartCoroutine(DestroyAnimation(0.3f));
            //move the particle effect object to the block and emit a burst of particles  
            particleEffects.transform.position = transform.position;
            particleEffects.EmitParticles(Color.white);
        }
        else {//if the block still has hits remaining
            //update the blocks colour and start the block hit animation
            UpdateBlockColour();
            StartCoroutine(HitAnimation(1.0f));
            //fire the OnBlockHit event 
            if (OnBlockkHit != null)
                OnBlockkHit();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    /// this is a public method that allows the spawner to set the amount of hits  ///
    /// that a block has when its spawned, it then sets the colour of the block    ///
    /// based on the hit points it spawns with                                     ///
    //////////////////////////////////////////////////////////////////////////////////

    public void SetHitsRemaining(int hits) {
        //hits is passd in by the spawner
        hitsRemaining = hits;
        UpdateBlockColour();
    }

    //////////////////////////////////////////////////////////////////////////////////
    /// this coroutine animates the block when it gets hit and doesn't need to be  /// 
    /// destroyed, the length of the animation is determined by the parameter, the ///
    /// animation works by scaling the block according to the animation curve, if  ///
    /// the block gets to zero hp during the animation, the coroutine breaks so    ///
    /// that the block destroy animation can play                                  ///
    //////////////////////////////////////////////////////////////////////////////////

    private IEnumerator HitAnimation(float animationLength) {
        float timer = 0;
        while (timer <= animationLength) {
            timer += Time.deltaTime;
            float scale = HitAnimationCurve.Evaluate(timer/animationLength);
            transform.localScale = Vector3.one * scale;
            if (destroying) yield break;
            yield return null;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    /// this coroutine animates the block when it gets to zero hp and needs to be  ///
    /// destroyed, the length of the animation is determined by the parameter, the ///
    /// animation works by scaling the block according to the animation curve. It  ///
    /// also sets destroying to true so this animation takes precedent over the    ///
    /// other hit animation. When the animation finishes, the block is destroyed   ///
    //////////////////////////////////////////////////////////////////////////////////

    private IEnumerator DestroyAnimation(float animationLength) {
        destroying = true;
        float timer = 0;
        while (timer <= animationLength) {
            timer += Time.deltaTime;
            float scale = DestroyAnimationCurve.Evaluate(timer/animationLength);
            transform.localScale = Vector3.one * scale;
            yield return null;
        }
        Destroy(gameObject);
    }
}
