﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

/////////////////////////////////////////////////////////////////////////////////////
/// This is a very simple class that simply ensures that the ball maintains       ///
/// a constant speed in the direction of its velocity                             ///
/////////////////////////////////////////////////////////////////////////////////////


public class Ball : MonoBehaviour {
    
    private Rigidbody ballRigidbody; 
    private const float moveSpeed = 10;

    private void Awake () {
	    ballRigidbody = GetComponent<Rigidbody>();
	}

    private void Update() {
        //takes the velocity of the rigidbody and keeps the direction but makes the speed constant 
        ballRigidbody.velocity = ballRigidbody.velocity.normalized * moveSpeed; 
    }


}
