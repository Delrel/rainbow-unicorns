﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

///////////////////////////////////////////////////////////////////////////////////////////
/// This class defines the OnBallReturn event and fires it when a ball collides with it ///
/// It also sets the ball the triggered the collision to inactive                       ///
///////////////////////////////////////////////////////////////////////////////////////////


public class BallCatcher : MonoBehaviour {

    public delegate void BallReturn();

    public static event BallReturn OnBallReturn;

    private void OnCollisionEnter (Collision other) {

        //no need to check what the collision is since, the way the game is set up, it should only collide with the balls
        other.collider.gameObject.SetActive(false);

        if(OnBallReturn != null)
            OnBallReturn();
    }
}
