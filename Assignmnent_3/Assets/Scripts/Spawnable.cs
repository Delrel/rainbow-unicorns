﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

////////////////////////////////////////////////////////////////////////////////////
/// This is the base class for the blocks and powerups in the game, it contains  ///
/// a method that moves the blocks/powerups down at the end of a round, based    ///
/// on the OnAllBallsReturned event                                              ///
////////////////////////////////////////////////////////////////////////////////////

public class Spawnable : MonoBehaviour {

    //this variable is implemented by the child classes Block and Powerup
    protected ParticleEffects particleEffects;


    //the method that moves everything down is subscribed to the OnAllBallsReturned event
    private void OnEnable() {
        PlayerShoot.OnAllBallsReturned += MoveDown;
    }

    private void OnDisable() {
        PlayerShoot.OnAllBallsReturned -= MoveDown;
    }

    private void MoveDown() {
        transform.position -= Vector3.forward * 2f;
        //stretch animation?
    }

}
