﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

///////////////////////////////////////////////////////////////////////////////////////////////
/// This class handles the UI elements in the game including displaying the score and high  ///
/// score, and the amount of balls the player has, it also saves the high score using       /// 
/// PlayerPrefs and actaully increments the score during the game and fires the OnGameReset ///
/// event that resets the entire game                                                       ///
///////////////////////////////////////////////////////////////////////////////////////////////

public class UIManager : MonoBehaviour {
    //the delegate and event used to reset the game 
    public delegate void ResetGame();
    public static event ResetGame OnResetGame;

    //references to the text objects used to display the score and high score
    [SerializeField] private Text currentScoreText;
    [SerializeField] private Text highScoreText;

    //the score and high score 
    private int currentScore;
    private int highScore;

    //reference to the text object used to diplay the current stored balls 
    [SerializeField] private Text storedBallsText;
    //the amount of stored balls, as taken from PlayerShoot
    private int storedBalls;
    //reference to the player shoot object, used to get the amount of stored balls 
    private PlayerShoot playerShoot;

    //references to the panel that overlays the screen when the game ends and the text objects that display the final score and high score
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private Text finalScoreText;
    [SerializeField] private Text finalHighScoreText;
    
    private void Awake() {
        playerShoot = FindObjectOfType<PlayerShoot>();
        //retrieve the high score from player prefs, with the default vakue 0 if it can be retrieved
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        UpdateStrings();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    /// this class ueses the OnAllBallsRedurned event to increment the score when a round ends ///
    /// and the GameEnd event to display a game over when the game ends                        ///
    /// //////////////////////////////////////////////////////////////////////////////////////////
    
    private void OnEnable() {
        PlayerShoot.OnAllBallsReturned += IncrementScore;
        GameOver.OnEndGame += EnableReset;
    }

    private void OnDisable() {
        PlayerShoot.OnAllBallsReturned -= IncrementScore;
        GameOver.OnEndGame -= EnableReset;
    }

    // storedBalls is updated in the update loop to dynamically show the count decreasing as the balls get shot
    private void Update() {
        storedBalls = playerShoot.StoredBalls;
        storedBallsText.text = "x" + storedBalls;
    }

    //this method simply updates the score and highscore text objects to diplay the latest scores
    private void UpdateStrings() {
        currentScoreText.text = currentScore.ToString();
        highScoreText.text = "High Score: " + highScore;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// this gets called when a round ends, it increments the score by one, and if the high score is less than the ///
    /// current score, increments that too. It then updates the text objects to display the corrrect scores        ///
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void IncrementScore() {
        currentScore++;
        if (highScore <= currentScore) {
            highScore = currentScore;
        }
        
        UpdateStrings();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /// this gets called when the game ends, it displays the gameover screen and saves the high score ///
    /// it also (by enabling the game over panel) enables the button used to reset the game           ///
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private void EnableReset() {
        gameOverPanel.SetActive(true);
        finalScoreText.text = "Score: " + currentScore;
        finalHighScoreText.text = "High Score: " + highScore;
        PlayerPrefs.SetInt("HighScore", highScore);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /// this gets called from a button in the scene, it fires the OnGameReset event so the entire game ///
    /// can be reset, then resets the current score to 0, and disables the game over panel             ///
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public void ResetTheGame() {
        //trigger reset event
        if (OnResetGame != null)
            OnResetGame();

        currentScore = 0;
        UpdateStrings();
        gameOverPanel.SetActive(false);
    }

    //in case we ever want to reset high score with a button 
    public void ResetScores() {
        PlayerPrefs.DeleteAll();
        highScore = 0;
    }
   
}
