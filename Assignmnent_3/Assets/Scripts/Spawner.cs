﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

///////////////////////////////////////////////////////////////////////////////////////////////
/// This class handles the spawning of blocks and powerups along a row                      ///
/// First it randomly decides if a spawnable object should be spawned in a particular       ///
/// column, and then randomly determines if that spawnable object should be a               ///
/// block or a powerup. It also increments the difficulty of the game with each row spawned ///
///////////////////////////////////////////////////////////////////////////////////////////////

public class Spawner : MonoBehaviour {

    //references to the prefabs that should be instatiated when a block/powerup spawns 
    public Block blockPrefab;
    public Powerup newBallPrefab;

    //the number of possible columns that spawnable items can be spawned in
    private const int Columns = 7;

    //the difficulty of the game 
    private int incrementDifficulty;

    //an array of all the spawned items still active when the game ends
    private Spawnable[] spawnedItems;

    ////////////////////////////////////////////////////////////////////////////////////////
    ///at the very beginning, this spawns the first row of spawnables and subscribes the ///
    /// SpawnRow method to the OnAllBallsReturned event, and the ResetSpanables method   ///
    /// to the OnResetGame event                                                         ///
    /// //////////////////////////////////////////////////////////////////////////////////// 
    private void OnEnable() {
        SpawnRow();
        PlayerShoot.OnAllBallsReturned += SpawnRow;
        UIManager.OnResetGame += ResetSpawnables;
    }

    private void OnDisable() {
        PlayerShoot.OnAllBallsReturned -= SpawnRow;
        UIManager.OnResetGame -= ResetSpawnables;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    /// This method actually spawns all the spawnables throughout the game, looping though   ///
    /// the number of available colummns, it first determines if something should be spawned ///
    /// and then determines if that should be a block or a powerup, if an object is spawned  ///
    /// its transform is made a child of the spawner transform finally it determines         ///
    /// if the difficulty should be increased                                                ///
    /// ////////////////////////////////////////////////////////////////////////////////////////

    private void SpawnRow() {
        for (int i = 0; i < Columns; i++) {
            //as the difficulty gets incremented the chance that something will spawn increases
            if (Random.Range(0, 100) > 30 + incrementDifficulty) continue; //if not in range, go to the next spot
            if (Random.Range(0, 100) <= 20) { //if in this range spawn a powerup
                var powerup = Instantiate(newBallPrefab, (transform.position + Vector3.right * i * 1.5f), Quaternion.identity);
                powerup.transform.parent = transform;
            }
            else {//otherwise spawn a blockand set its hits
                var block = Instantiate(blockPrefab, (transform.position + Vector3.right * i * 1.5f), Quaternion.identity);
                //as the difficulty gets incremented the number of hits the blocks spawn with will increase
                int hits = Random.Range(1, 3) + incrementDifficulty;
                block.SetHitsRemaining(hits);
                block.transform.parent = transform;
            }
        }

        //increment the difficulty of the game if in this range
        if (Random.Range(0, 100) <= 60) {
            incrementDifficulty++;
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    /// This method resets the spawned items and the difficulty of the game, the difficulty  ///
    /// is set back to 0 and all the spawned items that are still active are collected into  ///
    /// an array and destroyed                                                               ///
    /// ////////////////////////////////////////////////////////////////////////////////////////

    private void ResetSpawnables() {
        //reset difficulty
        incrementDifficulty = 0;
        //collect the spawned items by getting all the children of the spawner, then destroy them
        spawnedItems = GetComponentsInChildren<Spawnable>();
        foreach (var item in spawnedItems) {
            Destroy(item.gameObject);
        }
        SpawnRow();
    }
}
