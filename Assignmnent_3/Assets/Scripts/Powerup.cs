﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

////////////////////////////////////////////////////////////////////////////////////
/// This class handles the behaviour of the powerups in the game , it registers  ///
/// collisions from the balls. It triggers an events when the powerup is hit     ///
/// it is a child of the class Spawnable, which includes a method that moves     ///
/// the powerups down at the end of a round                                      ///
////////////////////////////////////////////////////////////////////////////////////

public class Powerup : Spawnable {
    //The delegate and event that fires when a powerup is hit
    public delegate void PowerupHit();
    public static event PowerupHit OnPowerupHit;

    //reference to the playerShoot class so it can add balls to the players pool
    private PlayerShoot playerShoot;

    private void Awake() {
        playerShoot = FindObjectOfType<PlayerShoot>();
        //a reference to the partcle effect object, the variable is inherited from Spawnable
        particleEffects = FindObjectOfType<ParticleEffects>();
    }

    /////////////////////////////////////////////////////////////////////////////////////
    /// This collision event adds a ball to the players pool of balls, and fires a    ///
    /// particle effect in the colour green. It also fires the OnPowerup hit event to ///
    /// inform the audio manager to play the powerup sound, finally after all that it ///
    /// destorys the game object                                                      ///
    /////////////////////////////////////////////////////////////////////////////////////

    private void OnTriggerEnter(Collider other) {
        //because of the physics settings/setup of the game, this object should only ever collide with balls
        playerShoot.CreateBall();
        particleEffects.transform.position = transform.position;
        particleEffects.EmitParticles(Color.green);
        if (OnPowerupHit != null)
            OnPowerupHit();
        Destroy(gameObject);
    }
}
