﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

////////////////////////////////////////////////////////////////////////////////////////////////
/// This class handles the user input during gameplay and the actual firing of the balls.    ///
/// It also defines and fires the event OnAllBallsReturned, which signals the end of a round ///
/// It also resets the balls on game reset                                                   ///
////////////////////////////////////////////////////////////////////////////////////////////////

public class PlayerShoot : MonoBehaviour {
    
    //the delegate and even that signal the end of a round i.e all balls are returned
    public delegate void AllBallsReturned();
    public static event AllBallsReturned OnAllBallsReturned;

    //the ball prefab to instantiate when a ball is created 
    public GameObject BallPrefab;
    
    // a vector3 to store the mouse position when the lmb is first pressed down
    private Vector3 mouseReferencePosition;
    //the vector3 to store the mouse position while the mouse is being dragged 
    private Vector3 currentMousePosition;
    //the direction the balls should shoot, based on the direction from the mouse ref position to the mouses current position
    private Vector3 directionVector;

    //the collection of balls the player has available and an int for storing how many balls have returned to the player
    private List<Rigidbody> balls = new List<Rigidbody>();
    public int StoredBalls;

    //bool to stop the player from shooting during the round 
    private bool canShoot = true;

    //reference to the line renderer on the player object used to diplay aim 
    private LineRenderer line;

    //reference to the button used to end the round if balls get stuck, plus the timer used to display activate it
    private float timer;
    private const int timeLimit = 10;
    [SerializeField] private GameObject returnBallsButton;

    //awake sets the initial poitions of the line renderer 
    void Awake () {
	    line = GetComponent<LineRenderer>();
	    line.SetPosition(0, transform.position + Vector3.down*0.5f);
	    line.SetPosition(1, transform.position);
        CreateBall();
	    canShoot = true;
	}

    //appropriate methods subscribed to appropriate events
    private void OnEnable() {
        BallCatcher.OnBallReturn += ReturnBall;
        UIManager.OnResetGame += ResetBalls;
    }

    private void OnDisable() {
        BallCatcher.OnBallReturn -= ReturnBall;
        UIManager.OnResetGame -= ResetBalls;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    /// the update loop takes a reference position of the mouse when the player presses lmb,   ///
    /// and determines if the line renderer should diplay the aim. If the player can shoot,    ///
    /// it also triggers the mouse shoot coroutine when the player releases lmb                ///
    /// if the player cannot shoot, that means a round is in progress, therefore it starts the ///
    /// timer for activatinhg the return balls button                                          ///
    //////////////////////////////////////////////////////////////////////////////////////////////  
	
	private void Update () {
	    if (Input.GetButtonDown("Fire1"))
	        mouseReferencePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

	    if (canShoot) {

	        if (Input.GetButton("Fire1")) {
	            DisplayAim(true);
	        }

	        if (Input.GetButtonUp("Fire1")) {
	            DisplayAim(false);
	            if (canShoot) {
	                StartCoroutine(ShootBalls(GetShootingVector()));
	            }
	        }
	    }
	    else {
	        timer += Time.deltaTime;
	        if (timer >= timeLimit)
	            returnBallsButton.SetActive(true);
	    }
        //for debugging purposes
        if(Input.GetKeyDown(KeyCode.Space))
            ReturnAllBalls();
	}

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// this method determines which direction the balls should fire when they shoot. It does   ///
    /// this by comparing the mouse reference position to the mouses current position           ///
    /// it takes this raw direction and removes the y component so the balls arent fired up     ///
    /// and down, and normalises the direction since the balls determine their own speed        ///
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private Vector3 GetShootingVector() {
        currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 difference = mouseReferencePosition - currentMousePosition;
        directionVector = new Vector3(difference.x, 0, difference.z);
        directionVector = Vector3.Normalize(directionVector);
        return directionVector;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// this method displays aim by settting the end of the line in the line renderer to the    ///
    /// direction the player is aiming, using a method similar to GetShootingVector, however it ///
    /// also takes into account the magnitude of the difference between mouse points to         ///
    /// determine how far away the end of the aim line should be                                ///
    /// if the line shouldnt be shown, it just resets the end of the line to transform.position ///
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private void DisplayAim(bool display) {
        currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 difference = mouseReferencePosition - currentMousePosition;
        Vector3 aimVector = transform.position + (GetShootingVector() * difference.magnitude * 10);

        Vector3 endPos = display ? aimVector : transform.position;
        line.SetPosition(1, endPos);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// this method creates balls by instantiating a ball prefaband adding it to the list of total balls ///
    /// this method gets called once at the start of the game, and every time a powerup is hit           ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void CreateBall() {
        var ball = Instantiate(BallPrefab, transform.position, Quaternion.identity);
        balls.Add(ball.GetComponent<Rigidbody>());
        ball.SetActive(false);
        StoredBalls++;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// this coroutine actually shoots the balls, it determines the amount of balls to shoot     ///
    /// using balls.count, and then runs a loop firing all those balls along the shooting vector ///
    /// with a 0.1 second delay between them, it then sets canShoot to false so the player can't ///
    /// shoot again until the round ends                                                         ///
    /// storedBalls is also decremented in the loop to keep track of how many balls get returnd  ///
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private IEnumerator ShootBalls(Vector3 shootingVector) {
        if (shootingVector == Vector3.zero) yield break;
        canShoot = false;
        int ballsToShoot = balls.Count;
        for (int i = 0; i < ballsToShoot; i++) {
            var storedBall = balls[i];
            storedBall.transform.position = transform.position +Vector3.down*0.5f;
            storedBall.velocity = Vector3.zero;
            storedBall.gameObject.SetActive(true);
            storedBall.AddForce(shootingVector);
            StoredBalls--;
            yield return new WaitForSeconds(0.1f);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// this method gets called when the OnBallReturn event is fired by the ball catcher, it     ///
    /// increments StoredBalls  when a ball is returned, and if the number of StoredBalls equals ///
    /// the count of all balls in the players pool, the OnAllBallsReturned event is fired,       ///
    /// canShoot is set to true, and the round ends                                              ///
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void ReturnBall() {
        StoredBalls++;
        if (StoredBalls != balls.Count || OnAllBallsReturned == null) return;
        returnBallsButton.SetActive(false);
        timer = 0;
        canShoot = true;
        OnAllBallsReturned();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// this method gets called from a button in the scene, if a ball gets stuck, this button    ///
    /// can be pressed, and all of the balls in the players pool are set incative,               ///
    /// canshoot is set to true, and the OnAllBallsReturned event is fired, ending the round     ///
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void ReturnAllBalls() {
        returnBallsButton.SetActive(false);
        timer = 0;
        foreach (var ball in balls) {
            ball.gameObject.SetActive(false);
        }

        StoredBalls = balls.Count;
        if (OnAllBallsReturned != null) OnAllBallsReturned();

        canShoot = true;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// this method gets called when the game resets, it destroys all balls in the players pool  ///
    /// and resets the list, it then creates one ball with which to begin the next game          ///
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void ResetBalls() {
        StoredBalls = 0;
        foreach (var ball in balls) {
            Destroy(ball.gameObject);
        }

        balls = new List<Rigidbody>();

        CreateBall();
        canShoot = true;
        returnBallsButton.SetActive(false);
        timer = 0;
    }
}
