﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

/////////////////////////////////////////////////////////////////////////////////////
/// This class handles the audio of the game, all the sounds play based on events ///
/// fired by other classes in te game. The audio manager object also loops the    ///
/// background music. We've made it a singleton so the music can play seamlessley ///
/// between scene transitions                                                     ///
/////////////////////////////////////////////////////////////////////////////////////

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

    public static AudioManager Instance = null;

    //references to the audio clips that will play during the game 
    public AudioClip BlockHitClip;
    public AudioClip BlockDestroyedClip;
    public AudioClip PowerupkHitClip;
    public AudioClip NewRowClip;

    //reference to the audio source component used to play the sound clips
    private AudioSource audioSource;
    
    /////////////////////////////////////////////////////////////////////////////////////
    /// The Start method makes the audio manager a singleton and retieves a reference ///
    /// to the audio source component on the object                                   ///
   /////////////////////////////////////////////////////////////////////////////////////
	private void Start () {

	    if (Instance == null) {
	        Instance = this;
	    } else {
	        Destroy(gameObject);
	    }
        DontDestroyOnLoad(gameObject);

	    audioSource = GetComponent<AudioSource>();
	}

    //use OnEnable to subscribe to the events used to play the appropriate sounds
    private void OnEnable() {
        Block.OnBlockkHit += PlayBlockHitSound; 
        Block.OnBlockDestroyed += PlayBlockDestroyedSound;
        Powerup.OnPowerupHit += PlayPowerupHitSound;
        PlayerShoot.OnAllBallsReturned += PlayNewRowSound;
        ButtonManager.OnButtonHit += PlayBlockHitSound;
    }

    //use OnDisable to unsubscribe from all the events 
    private void OnDisable() {
        Block.OnBlockkHit -= PlayBlockHitSound;
        Block.OnBlockDestroyed -= PlayBlockDestroyedSound;
        Powerup.OnPowerupHit -= PlayPowerupHitSound;
        PlayerShoot.OnAllBallsReturned -= PlayNewRowSound;
        ButtonManager.OnButtonHit -= PlayBlockHitSound;
    }

    private void PlayBlockHitSound() {
        audioSource.PlayOneShot(BlockHitClip);
    }

    private void PlayBlockDestroyedSound() {
        audioSource.PlayOneShot(BlockDestroyedClip);
    }

    private void PlayPowerupHitSound() {
        audioSource.PlayOneShot(PowerupkHitClip);
    }

    private void PlayNewRowSound() {
        audioSource.PlayOneShot(NewRowClip);
    }
	
}
