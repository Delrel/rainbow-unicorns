﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

///////////////////////////////////////////////////////////////////////////////////////////
/// This class defines the OnEndGame event and fires it when a block collides with it   ///
///////////////////////////////////////////////////////////////////////////////////////////

public class GameOver : MonoBehaviour {

    public delegate void EndGame();
    public static event EndGame OnEndGame;

    private void OnCollisionEnter(Collision other) {
        //because of the physics settings of the game, this object should only ever collide with blocks
        if (OnEndGame != null)
            OnEndGame();
    }
}
