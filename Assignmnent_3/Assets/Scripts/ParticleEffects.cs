﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

////////////////////////////////////////////////////////////////////////////////////////////
/// This class contains a method to emit particles from the partile effect object in the ///
/// colour thats passed into the parameter                                               ///
////////////////////////////////////////////////////////////////////////////////////////////

public class ParticleEffects : MonoBehaviour {

    private ParticleSystem particleEffectSystem; //a reference to the Particle System on the object
    private ParticleSystem.ColorOverLifetimeModule colorOverLifetime; //a reference to the colour over lifetime component of the particle
    
	private void Start () {
	    particleEffectSystem = GetComponent<ParticleSystem>();
	    colorOverLifetime = particleEffectSystem.colorOverLifetime;
	}

    //emits particles with a colour over lifetime, the parameter defines the starting colour of the gradient
    public void EmitParticles(Color col) {
        ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient(col, Color.clear);
        colorOverLifetime.color = gradient;

        particleEffectSystem.Play();
    }
}
