﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour: Kristine Wong Yeh Kay s3587133
 *
 */

//////////////////////////////////////////////////////////////////////////////////////
/// This class handles the first background, it calculates the edges of the sprite ///
/// and moves the background based on the OnAllBallsReturned event.                ///
/// If the sprite reaches the appropriate edge, it sets the sprite renderer        ///
/// colour to clear                                                                ///
//////////////////////////////////////////////////////////////////////////////////////

public class FirstBackground : MonoBehaviour
{
    private float topEdge;
    private float bottomEdge;
    private Vector3 distanceBetween;
    private SpriteRenderer spriteRenderer;
    private Vector3 startPosition;



    // Use this for initialization
    void Start()
    {
        calculateEdges();

        distanceBetween = new Vector3(topEdge - bottomEdge, 0f, 0f);

        startPosition = transform.position;

    }

    private void calculateEdges()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        topEdge = transform.position.z + spriteRenderer.bounds.extents.z / 3f;
        bottomEdge = transform.position.z - spriteRenderer.bounds.extents.z / 3f;

    }

    //this method is subscribed to the OnAllBallsReturned event so the background moves when the round ends 
    private void moveBackground()
    {
        if (passedEdge()) //if the background is no longer needed
        {
            //gameObject.SetActive(false);
            spriteRenderer.color = Color.clear;

        }
        else {
            transform.localPosition += Vector3.back;
        }
    }


    //remove this and call movebackground function when the new block appears, also edit 
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            moveBackground();
        }
    }

    //this method checks to see if the background needs to be turned off
    private bool passedEdge()
    {
        if (transform.position.z < bottomEdge-5)
        {
            return true;
        }
        else
            return false;
    }

    //this method is subscribed to the OnResetGame event to reset the background when the game resets
    private void ResetBackground() {
        transform.position = startPosition;
        spriteRenderer.color = Color.white;
    }

    private void OnEnable()
    {
        PlayerShoot.OnAllBallsReturned += moveBackground;
        UIManager.OnResetGame += ResetBackground;
    }

    private void OnDisable()
    {
        PlayerShoot.OnAllBallsReturned -= moveBackground;
        UIManager.OnResetGame -= ResetBackground;
    }
}
