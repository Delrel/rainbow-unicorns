﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 * authour: Kristine Wong Yeh Kay s3587133
 *
 */

/////////////////////////////////////////////////////////////////////////////////////
/// This class handles the endless scrolling background, it calculates the        ///
/// edges of the sprite and moves the background based on the OnAllBallsReturned  ///
/// event. If the sprite reaches the appropriate edge, it loops back around       ///
/////////////////////////////////////////////////////////////////////////////////////

public class Background : MonoBehaviour {
    
    private float topEdge;
    private float bottomEdge;
    private Vector3 distanceBetween;



	// Use this for initialization
	void Start () {
        calculateEdges();

        distanceBetween = new Vector3(topEdge - bottomEdge, 0f, 0f);
	}

    private void calculateEdges()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        topEdge = transform.position.z + spriteRenderer.bounds.extents.z / 3f;
        bottomEdge = transform.position.z - spriteRenderer.bounds.extents.z / 3f;

    }
	
    //this method is subscribed to the OnAllBallsReturned event so the background moves when the round ends
    private void moveBackground()
    {
        transform.localPosition += Vector3.back;

        if (passedEdge())
        {
            moveToOtherEdge();

        }
    }


    //remove this and call movebackground function when the new block appears, also edit 
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            moveBackground();
        }
    }

    //this method checks to see if the background needs to be reset
    private bool passedEdge()
    {
        if (transform.position.z < bottomEdge)
        {
            return true;
        }
        else
            return false;

    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    ///this method is called when the backround needs to loop, it is also subscribes to the ///
    /// OnResetGame event so the background can be reset when the game is reset             ///
    /////////////////////////////////////////////////////////////////////////////////////////// 
    private void moveToOtherEdge()
    {
        var post = transform.position;
        transform.position = new Vector3(-5.1f, 0.01f, -28.9f);
    }

    private void OnEnable()
    {
        PlayerShoot.OnAllBallsReturned += moveBackground;
        UIManager.OnResetGame += moveToOtherEdge;
    }

    private void OnDisable()
    {
        PlayerShoot.OnAllBallsReturned -= moveBackground;
        UIManager.OnResetGame -= moveToOtherEdge;
    }
}
