﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 *
 * authour Rebecca Dilella s3663787 
 *
 */

///////////////////////////////////////////////////////////////////////////////////////////
/// This class contains a few public methods that are called from the editor using      ///
/// ui events called from buttons. It also fires an event to let the audio manager      ///
/// know to play a sound                                                                ///
///////////////////////////////////////////////////////////////////////////////////////////

public class ButtonManager : MonoBehaviour {
    //the delegate and event used to infom the audiomanager when a button is pressed 
    public delegate void ButtonHit();
    public static event ButtonHit OnButtonHit;

    public void StartTheGame() {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void GoToMainMenu() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void QuitTheGame() {
        Application.Quit();
        print("Quit button pressed");
    }

    public void PlayButtonSound() {
        if (OnButtonHit != null)
            OnButtonHit();
    } 
}
