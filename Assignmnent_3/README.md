# Game Studio 2
Rainbow Unicorns Assignment 3

Team members:
Kristine Wong Yeh Kay s3587133
Rebecca DiLella s3663787

=====================================================================================================================================================================================================
Contributions:
	Assignment 2:
		Kristine:			
		- Implemented the game mechanics: moving background, block hp diplay
		- Sourced the background images and sounds

		Rebecca:
		- Implemented the game mechanics: player shoot, block/powerup spawn and move down, score display
		- Implemented sounds
		- Created 3d models, (acorns/ cloud blocks/ squirrel), and the Title and GameOver sprites	
					
====================================================================================================================================================================================================			
Downloads:
	Audio:
	Bounce Sound - "tennis-ball-bounce_zkJWcjEO" by Audioblocks.com <https://www.audioblocks.com/stock-audio/tennis-ball-bounce-100055.html>
	New Row Sound - "Mario Jumping Sound" by Mike Koenig <http://soundbible.com/1601-Mario-Jumping.html>
	Throwing Sound - "Sword Swing B - Strong 02.wav" by Glaneur de sons <https://freesound.org/people/Glaneur%20de%20sons/sounds/420612/>
	Block Destroyed Sound - "Poof of Smoke" by Planman <https://freesound.org/people/Planman/sounds/208111/> 
	PowerupHit Sound - "Coins 1" by ProjectsU012 <https://freesound.org/people/ProjectsU012/sounds/341695/>
	Background Music - "happy cave" by ADnova <https://freesound.org/people/ADnova/sounds/435531/>
	
	Images:
	Background 1 - "Animated Cartoon Cloudy Sky with Tree and Ground and hills" by Footageisland <https://es.videoblocks.com/video/animated-cartoon-cloudy-sky-with-tree-and-ground-and-hills-oemwibt>
			~edited by Kristine
	Background 2 - "Cartoon Clouds and Blue Sky" by 3dViz <https://www.shutterstock.com/pt/video/clip-1748566-cartoon-clouds-blue-sky>
			~edited by Kristine


